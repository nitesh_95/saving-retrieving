package com.photoMania;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Contact")
public class ManagerLogin {
	@Id
	@Column(name = "mobile")
	private String mobile;
	@Column(name = "name")
	private String name;
	

	@Column(name = "email")
	private String email;
	@Column(name = "remarks")
	private String remarks;
	@Column(name = "address")
	private String address;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "ManagerLogin [name=" + name + ", mobile=" + mobile + ", email=" + email + ", remarks=" + remarks
				+ ", address=" + address + "]";
	}
	
	
	
}
