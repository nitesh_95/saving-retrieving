package com.photoMania;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class Controller {
	
	@Autowired
	private ManagerImpl managerImpl;
	 @RequestMapping(value= "/contact/all")
	    public List<ManagerLogin> getEmployees() {
	        return managerImpl.findAll();
	    }
	
	@RequestMapping("/save")
	public LoginResponse save(@RequestBody ManagerLogin login) {
		LoginResponse res = new LoginResponse();
		boolean isupdated = managerImpl.save(login);
		if(isupdated) {
			res.setReason("success");
			res.setStatus("00");
		}else {
			res.setReason("failed");
			res.setStatus("s01");
		}
		 return res;
	 }

}
