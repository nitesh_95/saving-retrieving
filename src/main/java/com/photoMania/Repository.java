package com.photoMania;

import org.springframework.data.jpa.repository.JpaRepository;

public interface Repository extends JpaRepository<ManagerLogin, String>{

}
