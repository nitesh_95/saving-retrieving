package com.photoMania;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhotoManiaContactApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhotoManiaContactApplication.class, args);
	}

}
